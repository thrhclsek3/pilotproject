package org.jeonfeel.pilotproject1

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import org.jeonfeel.pilotproject1.data.database.AppDatabase
import org.jeonfeel.pilotproject1.data.sharedpreferences.PreferenceManager
import org.jeonfeel.pilotproject1.utils.CheckNetworkOnce

@HiltAndroidApp
class AppApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        PreferenceManager.create(this.applicationContext)
    }
}