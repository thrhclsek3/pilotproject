package org.jeonfeel.pilotproject1.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jeonfeel.pilotproject1.R
import org.jeonfeel.pilotproject1.data.database.entity.Favorite
import org.jeonfeel.pilotproject1.data.error.NETWORK_ERROR
import org.jeonfeel.pilotproject1.data.remote.model.StarbucksMenuDTO
import org.jeonfeel.pilotproject1.data.sharedpreferences.PreferenceManager
import org.jeonfeel.pilotproject1.repository.MainRepository
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.arrayListOf
import kotlin.collections.filter
import kotlin.collections.first
import kotlin.collections.maxOf
import kotlin.collections.set
import kotlin.collections.sortBy
import kotlin.collections.sortByDescending
import kotlin.collections.toList
import kotlin.math.ceil

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    private val TAG = "MainActivityViewModel"
    private val originalList: ArrayList<ArrayList<StarbucksMenuDTO>> = arrayListOf()

    private val _categoryListLiveData: MutableLiveData<List<String>> =
        MutableLiveData<List<String>>()
    val categoryListLiveData: LiveData<List<String>> get() = _categoryListLiveData

    private val _starbucksMenuLiveData: MutableLiveData<ArrayList<ArrayList<StarbucksMenuDTO>>> =
        MutableLiveData<ArrayList<ArrayList<StarbucksMenuDTO>>>()
    val starbucksMenuLiveData: LiveData<ArrayList<ArrayList<StarbucksMenuDTO>>> get() = _starbucksMenuLiveData

    private val _favoriteLiveData: MutableLiveData<HashMap<String, Int>> =
        MutableLiveData<HashMap<String, Int>>()
    val favoriteLiveData: LiveData<HashMap<String, Int>> get() = _favoriteLiveData

    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData

    private val _showSnackBar = MutableLiveData<Int>()
    val showSnackBar: LiveData<Int> get() = _showSnackBar

    var tempNutritionalInformation = HashMap<String, Int>()

    fun requestStarbucksMenus() {
        viewModelScope.launch(Dispatchers.IO) {
            parseData()
        }
    }

    private suspend fun parseData() {
        _loadingLiveData.postValue(true)
        val response = mainRepository.getStarbucksMenuListCall().getStarbucksMenu()
        if (response.isSuccessful) {
            val category = response.body()?.keySet()?.toList() ?: emptyList()
            val allStarbucksMenuDTO = ArrayList<StarbucksMenuDTO>()
            val gson = Gson()
            _categoryListLiveData.postValue(category)
            val favoritesHashMap = HashMap<String, Int>()
            val favoriteList = mainRepository.getFavoritesInstance().selectAll()

            for (elements in favoriteList) {
                favoritesHashMap[elements.productCD] = 0
            }
            _favoriteLiveData.postValue(favoritesHashMap)

            for (element in category) {
                val starbucksMenuDTOs = ArrayList<StarbucksMenuDTO>()
                val categoryJsonObject = response.body()?.getAsJsonObject(element) ?: JsonObject()
                val starbucksMenuJsonArray = categoryJsonObject.getAsJsonArray("list")

                for (i in 0 until starbucksMenuJsonArray?.size()!!) {
                    val sampleItem =
                        gson.fromJson(starbucksMenuJsonArray[i], StarbucksMenuDTO::class.java)
                    starbucksMenuDTOs.add(sampleItem)
                    allStarbucksMenuDTO.add(sampleItem)
                }
                originalList.add(starbucksMenuDTOs)
            }
            originalList.add(0, allStarbucksMenuDTO)
            updateSetting()
            _loadingLiveData.postValue(false)
        } else {
            _showSnackBar.postValue(NETWORK_ERROR)
            _loadingLiveData.postValue(false)
        }
    }

    fun checkFavorite(productCD: String, favoriteIsChecked: Boolean) {
        val favoriteHashMap = favoriteLiveData.value ?: HashMap()
        if (favoriteIsChecked && favoriteHashMap[productCD] == null) {
            favoriteHashMap[productCD] = 0
            _favoriteLiveData.value = favoriteHashMap
            insertFavorite(productCD)
        } else if (!favoriteIsChecked && favoriteHashMap[productCD] != null) {
            favoriteHashMap.remove(productCD)
            _favoriteLiveData.value = favoriteHashMap
            deleteFavorite(productCD)
        }
    }

    private fun insertFavorite(productCD: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val favorite = Favorite(productCD)
            mainRepository.getFavoritesInstance().insert(favorite)
        }
    }

    private fun deleteFavorite(productCD: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val favorite = Favorite(productCD)
            mainRepository.getFavoritesInstance().delete(favorite)
        }
    }

    /**
     * 세팅
     * */
    fun updateSetting(selectedTabPosition: Int = 0) {
        val sortInfo = PreferenceManager.sortKind
        val isCaffeine = PreferenceManager.isCaffeine
        val resultList: ArrayList<ArrayList<StarbucksMenuDTO>> = ArrayList()

        for (i in 0 until originalList.size) {
            var tempList = sortList(sortInfo, originalList[i])
            tempList = filterCaffeine(tempList, isCaffeine)
//            if (i == selectedTabPosition) {
//                tempList = filterNutritionalInformation(
//                    getApplication(),
//                    tempList,
//                    tempNutritionalInformation
//                )
//            }
            resultList.add(tempList)
        }
        _starbucksMenuLiveData.postValue(resultList)
    }

    // sorting List
    private fun sortList(
        sortInfo: Int,
        originalList: List<StarbucksMenuDTO>
    ): ArrayList<StarbucksMenuDTO> {
        val resultList = ArrayList<StarbucksMenuDTO>()
        resultList.addAll(originalList)

        when (sortInfo) {
            PreferenceManager.SORT_LOW_KCAL -> resultList.sortBy { it.kcal.toInt() }
            PreferenceManager.SORT_HIGH_KCAL -> resultList.sortByDescending { it.kcal.toInt() }
        }
        return resultList
    }

    // caffeine filter
    private fun filterCaffeine(
        resultList: ArrayList<StarbucksMenuDTO>,
        onCaffeineFilter: Boolean,
    ): ArrayList<StarbucksMenuDTO> {
        if (onCaffeineFilter) {
            return resultList.filter { it.caffeine.toInt() == 0 } as ArrayList
        }
        return resultList
    }

    // 단백질, 지방, 설탕 filter
    private fun filterNutritionalInformation(
        context: Context,
        resultList: List<StarbucksMenuDTO>,
        nutritionalInformation: HashMap<String, Int>
    ): ArrayList<StarbucksMenuDTO> {
        if (nutritionalInformation.size != 0) {
            return resultList.filter {
                ceil(it.protein.toFloat()).toInt() in nutritionalInformation[context.getString(R.string.nutritionalInformation_lowProtein_key)]!!..nutritionalInformation[context.getString(
                    R.string.nutritionalInformation_highProtein_key
                )]!!
                        && ceil(it.fat.toFloat()).toInt() in nutritionalInformation[context.getString(
                    R.string.nutritionalInformation_lowFat_key
                )]!!..nutritionalInformation[context.getString(R.string.nutritionalInformation_highFat_key)]!!
                        && ceil(it.sugars.toFloat()).toInt() in nutritionalInformation[context.getString(
                    R.string.nutritionalInformation_lowSugar_key
                )]!!..nutritionalInformation[context.getString(R.string.nutritionalInformation_highSugar_key)]!!
            } as ArrayList
        }
        return resultList as ArrayList
    }

    fun getProteinMaxValue(tabPosition: Int): Float {
        return originalList[tabPosition].maxOf { it.protein.toFloat() }
    }

    fun getFatMaxValue(tabPosition: Int): Float {
        return originalList[tabPosition].maxOf { it.fat.toFloat() }
    }

    fun getSugarsMaxValue(tabPosition: Int): Float {
        return originalList[tabPosition].maxOf { it.sugars.toFloat() }
    }

    /**
     * FCM, DynamicLink
     * */
    fun getFcmAndDynamicLinkDTO(productCD: String): StarbucksMenuDTO {
        return originalList[0].first { it.product_CD == productCD }
    }

    fun networkNotConnect() {
        _showSnackBar.postValue(NETWORK_ERROR)
    }

    fun makeZero() {
        _showSnackBar.value = 0
    }
}

// ¯\_(ツ)_/¯ heum..