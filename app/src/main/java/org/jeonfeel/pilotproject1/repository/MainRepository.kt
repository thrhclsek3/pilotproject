package org.jeonfeel.pilotproject1.repository

import org.jeonfeel.pilotproject1.data.database.AppDatabase
import org.jeonfeel.pilotproject1.data.remote.api.RetrofitService
import retrofit2.Retrofit


class MainRepository (private val db: AppDatabase,private val retrofitService: RetrofitService) {

    private val TAG = MainRepository::class.java.simpleName

    fun getStarbucksMenuListCall() = retrofitService

    fun getFavoritesInstance() = db.favoriteDao()
}
