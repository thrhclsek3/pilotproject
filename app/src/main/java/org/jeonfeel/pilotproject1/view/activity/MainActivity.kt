package org.jeonfeel.pilotproject1.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import org.jeonfeel.pilotproject1.R
import org.jeonfeel.pilotproject1.data.error.NETWORK_ERROR
import org.jeonfeel.pilotproject1.databinding.ActivityMainBinding
import org.jeonfeel.pilotproject1.utils.CheckNetworkOnce
import org.jeonfeel.pilotproject1.utils.NetworkMonitorUtil
import org.jeonfeel.pilotproject1.view.adapter.RecyclerViewMainListener
import org.jeonfeel.pilotproject1.view.adapter.ViewPagerAdapter
import org.jeonfeel.pilotproject1.view.fragment.FragmentSettingMain
import org.jeonfeel.pilotproject1.viewmodel.MainViewModel
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : FragmentActivity(), FragmentSettingMain.FragmentSettingListener,
    RecyclerViewMainListener {

    private val mainActivityViewModel: MainViewModel by viewModels()
    @Inject lateinit var viewPagerAdapter: ViewPagerAdapter
    private val TAG = MainActivity::class.java.simpleName
    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val networkCheck = NetworkMonitorUtil(this@MainActivity)
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == RESULT_OK) {
                val intent = result.data
                val productCD: String = intent!!.getStringExtra("productCD").toString()
                val favoriteIsChecked = intent.getBooleanExtra("favoriteIsChecked", false)

                mainActivityViewModel.checkFavorite(
                    productCD,
                    favoriteIsChecked
                )
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        initActivity()
    }

    override fun onResume() {
        super.onResume()
//        networkCheck.register()
    }

    /**
     * 액티비티 초기화
     */
    private fun initActivity() {
        initObserver()
        initListener()


        if (CheckNetworkOnce.checkNetwork(this) == null) {
            mainActivityViewModel.networkNotConnect()
        } else {
            mainActivityViewModel.requestStarbucksMenus()
        }
    }

    /**
     * 옵저버
     */
    private fun initObserver() {

        mainActivityViewModel.loadingLiveData.observe(this) {
            loading(it)
            checkPushAndDynamicLink(intent)
        }

        mainActivityViewModel.showSnackBar.observe(this) {
            when (mainActivityViewModel.showSnackBar.value) {
                NETWORK_ERROR -> {
                    val snackBar = Snackbar.make(
                        binding.constMain,
                        "네트워크 상태를 확인해 주세엽",
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackBar.setAction(
                        "재시도"
                    ) {
                        if (CheckNetworkOnce.checkNetwork(this) == null) {
                            mainActivityViewModel.networkNotConnect()
                        } else {
                            mainActivityViewModel.requestStarbucksMenus()
                        }
                    }
                    snackBar.show()
                }
            }
        }

        mainActivityViewModel.categoryListLiveData.observe(this) {
            initViewPager(it)
        }

        mainActivityViewModel.favoriteLiveData.observe(this) {
            viewPagerAdapter.setFavorites(it)
        }

        mainActivityViewModel.starbucksMenuLiveData.observe(this) {
            viewPagerAdapter.setItem(it)
        }
    }

    /**
     * 리스너
     */
    @SuppressLint("ClickableViewAccessibility")
    private fun initListener() {
        binding.tlMain.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (viewPagerAdapter.getSelectedTabPosition() != tab!!.position) {
                    mainActivityViewModel.tempNutritionalInformation.clear()
                }
                viewPagerAdapter.setSelectedTabPosition(tab.position)
                searchRecyclerviewItem()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        binding.etSearchMain.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewPagerAdapter.search(str.toString())
            }

            override fun afterTextChanged(str: Editable?) {
            }
        })

        binding.btnAdjustMain.setOnClickListener {
            val fragmentSettingSlideUpAnim =
                AnimationUtils.loadAnimation(this, R.anim.anim_slide_up)

            binding.flSettingMain.animation = fragmentSettingSlideUpAnim
            binding.flSettingMain.visibility = View.VISIBLE

            Log.d(TAG, mainActivityViewModel.tempNutritionalInformation.toString())

            val fragment = FragmentSettingMain.newInstance()
            fragment.setSliderValue(
                mainActivityViewModel.getProteinMaxValue(binding.tlMain.selectedTabPosition),
                mainActivityViewModel.getFatMaxValue(binding.tlMain.selectedTabPosition),
                mainActivityViewModel.getSugarsMaxValue(binding.tlMain.selectedTabPosition),
                mainActivityViewModel.tempNutritionalInformation
            )
            supportFragmentManager
                .beginTransaction()
                .replace(binding.flSettingMain.id, fragment)
                .commit()

            val bundle = Bundle()
            bundle.putString("event_name", "clickBtnAdjust")
            firebaseAnalytics.logEvent("click_btn_adjust", bundle)
        }

        binding.btnMessageBoxMain.setOnClickListener {
            startActivity(Intent(this, FcmBoxActivity::class.java))
        }
    }

    private fun initViewPager(category: List<String>) {
        binding.viewPager2.adapter = viewPagerAdapter
        TabLayoutMediator(binding.tlMain, binding.viewPager2) { tab, position ->
            addTabLayoutCategory(position - 1, tab, category)
        }.attach()
    }

    private fun searchRecyclerviewItem() {
        if (binding.etSearchMain.length() != 0) {
            val currentString = binding.etSearchMain.text.toString()
            viewPagerAdapter.search(currentString)
        }
    }

    private fun addTabLayoutCategory(position: Int, tab: TabLayout.Tab, category: List<String>) {
        if (position == -1) {
            tab.text = "All"
        } else {
            tab.text = category[position]
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    // 앱이 foreGround 상태일 떄 다이나믹 링크 or FCM 동작 처리
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        checkPushAndDynamicLink(intent)
    }

    private fun checkPushAndDynamicLink(intent: Intent?) {
        if (mainActivityViewModel.starbucksMenuLiveData.value != null) {
            Firebase.dynamicLinks
                .getDynamicLink(intent)
                .addOnSuccessListener(this) { pendingDynamicLinkData ->
                    var deeplink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deeplink = pendingDynamicLinkData.link
                    }
                    when {
                        deeplink != null -> {
                            val sanitizer = UrlQuerySanitizer(deeplink.toString())
                            val productCD = sanitizer.getValue("product_CD") ?: ""
                            val newIntent = Intent(this, StarbucksMenuDetailActivity::class.java)
                            newIntent.putExtra(
                                "starbucksMenuDTO",
                                mainActivityViewModel.getFcmAndDynamicLinkDTO(productCD)
                            )
                            startForResult.launch(newIntent)
                        }
                        intent?.extras != null -> {
                            val productCD = intent.extras!!.getString("product_CD") ?: ""
                            val newIntent = Intent(this, StarbucksMenuDetailActivity::class.java)
                            newIntent.putExtra(
                                "starbucksMenuDTO",
                                mainActivityViewModel.getFcmAndDynamicLinkDTO(productCD)
                            )
                            startForResult.launch(newIntent)
                        }
                    }
                }
                .addOnFailureListener(this) { e -> Log.e(TAG, "getDynamicLink:onFailure", e) }
        }
    }

    private fun loading(isLoading: Boolean) {
        if (isLoading) {
            binding.testShimmer.startShimmer()
        } else {
            binding.testShimmer.stopShimmer()
            binding.testShimmer.visibility = View.GONE
            binding.etSearchMain.visibility = View.VISIBLE
            binding.btnAdjustMain.visibility = View.VISIBLE
            binding.viewPager2.visibility = View.VISIBLE
            binding.tlMain.visibility = View.VISIBLE
            binding.btnMessageBoxMain.visibility = View.VISIBLE
            binding.ivSearchMain.visibility = View.VISIBLE
        }
    }

    override fun frameLayoutGone() {
        val fragmentSettingSlideDownAnim =
            AnimationUtils.loadAnimation(this, R.anim.anim_slide_down)
        fragmentSettingSlideDownAnim.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                val fragment = supportFragmentManager.findFragmentById(binding.flSettingMain.id)
                if (fragment != null)
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        binding.flSettingMain.animation = fragmentSettingSlideDownAnim
        binding.flSettingMain.visibility = View.GONE
    }

    override fun updateSettingImmediately(nutritionalInformation: HashMap<String, Int>) {
        mainActivityViewModel.tempNutritionalInformation = nutritionalInformation
        Log.d(TAG, mainActivityViewModel.tempNutritionalInformation.toString())
        mainActivityViewModel.updateSetting(binding.tlMain.selectedTabPosition)
    }

    override fun startForActivityResult(intent: Intent) {
        startForResult.launch(intent)
    }
}
