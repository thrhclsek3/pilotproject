package org.jeonfeel.pilotproject1.view.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.RangeSlider
import org.jeonfeel.pilotproject1.R
import org.jeonfeel.pilotproject1.data.sharedpreferences.PreferenceManager
import org.jeonfeel.pilotproject1.databinding.FragmentSettingMainBinding
import kotlin.math.ceil


class FragmentSettingMain : Fragment() {

    private val TAG = FragmentSettingMain::class.java.simpleName
    private var _binding: FragmentSettingMainBinding? = null
    private val binding get() = _binding
    private var sortInfo = 0
    private var isCaffeine = false
    private val listener by lazy {
        requireActivity() as FragmentSettingListener
    }
    private val sliders by lazy {
        arrayOf(binding?.sliderProtein, binding?.sliderFat, binding?.sliderSugar)
    }
    private val tvSliderLows by lazy {
        arrayOf(binding?.tvSliderProteinLow, binding?.tvSliderFatLow, binding?.tvSugarLow)
    }
    private val tvSliderHighs by lazy {
        arrayOf(binding?.tvSliderProteinHigh, binding?.tvSliderFatHigh, binding?.tvSugarHigh)
    }
    private val lowKeyList by lazy {
        arrayOf(
            requireActivity().getString(R.string.nutritionalInformation_lowProtein_key),
            requireActivity().getString(R.string.nutritionalInformation_lowFat_key),
            requireActivity().getString(R.string.nutritionalInformation_lowSugar_key)
        )
    }
    private val highKeyList by lazy {
        arrayOf(
            requireActivity().getString(R.string.nutritionalInformation_highProtein_key),
            requireActivity().getString(R.string.nutritionalInformation_highFat_key),
            requireActivity().getString(R.string.nutritionalInformation_highSugar_key)
        )
    }
    private var nutritionalInformation = HashMap<String, Int>()
    private lateinit var maxNutritionList: Array<Float>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSettingMainBinding.inflate(inflater, container, false)

        initSetting()
        initListener()

        return _binding?.root
    }

    private fun initSetting() {
        binding?.switchCaffeine?.isChecked = PreferenceManager.isCaffeine

        sortInfo = PreferenceManager.sortKind
        when (sortInfo) {
            PreferenceManager.SORT_BASIC -> {
                binding?.rbtnFragmentSettingMainNormal?.isChecked = true
            }
            PreferenceManager.SORT_HIGH_KCAL -> {
                binding?.rbtnFragmentSettingMainHighkcal?.isChecked = true
            }
            PreferenceManager.SORT_LOW_KCAL -> {
                binding?.rbtnFragmentSettingMainLowkcal?.isChecked = true
            }
        }
        initSlider()
    }

    /**
     * 리스너
     * */
    private fun initListener() {
        binding?.btnFragmentSettingMainClose?.setOnClickListener {
            fragmentFinish()
        }

        binding?.btnAdmitFragmentSettingMain?.setOnClickListener {
            PreferenceManager.isCaffeine = isCaffeine
            PreferenceManager.sortKind = sortInfo
            val proteinValues = binding?.sliderProtein?.values
            val fatValues = binding?.sliderFat?.values
            val sugarValues = binding?.sliderSugar?.values

            saveCurrentSetting(
                listOf(proteinValues, fatValues, sugarValues)
            )

            listener.updateSettingImmediately(nutritionalInformation)
            fragmentFinish()
        }

        binding?.rgFragmentSettingMain?.setOnCheckedChangeListener { radioGroup: RadioGroup, i: Int ->
            when (radioGroup.checkedRadioButtonId) {
                binding?.rbtnFragmentSettingMainLowkcal?.id -> sortInfo =
                    PreferenceManager.SORT_LOW_KCAL
                binding?.rbtnFragmentSettingMainHighkcal?.id -> sortInfo =
                    PreferenceManager.SORT_HIGH_KCAL
                binding?.rbtnFragmentSettingMainNormal?.id -> sortInfo =
                    PreferenceManager.SORT_BASIC
            }
        }

        for (i in sliders.indices) {
            sliders[i]?.addOnChangeListener(RangeSlider.OnChangeListener { slider, _, _ ->
                val sliderValue = slider.values
                val minValue = sliderValue[0]
                val maxValue = sliderValue[1]
                tvSliderLows[i]?.text = minValue.toInt().toString()
                tvSliderHighs[i]?.text = maxValue.toInt().toString()
            })
        }

        binding?.switchCaffeine?.setOnCheckedChangeListener { button, _ ->
            isCaffeine = if (button.isChecked) {
                PreferenceManager.CONTAINS_CAFFEINE
            } else {
                PreferenceManager.NOT_CONTAINS_CAFFEINE
            }
        }

        binding?.btnResetFragmentSettingMain?.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(requireActivity())
            dialogBuilder.setTitle("초기화")
                .setMessage("ㄹㅇ?")
                .setPositiveButton("ㅇㅇ") { _, _ ->
                    PreferenceManager.resetPreference()
                    nutritionalInformation.clear()
                    initSetting()
                    listener.updateSettingImmediately(nutritionalInformation)
                    fragmentFinish()
                    listener.frameLayoutGone()
                }
                .setNegativeButton("ㄴㄴ") { _, _ -> }

            val dialog = dialogBuilder.create()
            dialog.show()
        }
    }

    private fun initSlider() {
        for (i in sliders.indices) {
            when {
                maxNutritionList[i] == 0.0f -> {
                    sliders[i]?.visibility = View.GONE
                    tvSliderLows[i]?.text = "0"
                    tvSliderHighs[i]?.text = ceil(maxNutritionList[i]).toInt().toString()
                }
                nutritionalInformation.size != 0 -> {
                    sliders[i]?.apply {
                        labelBehavior = LabelFormatter.LABEL_GONE
                        valueTo = maxNutritionList[i]
                        values = listOf(
                            nutritionalInformation[lowKeyList[i]]!!.toFloat(),
                            nutritionalInformation[highKeyList[i]]!!.toFloat()
                        )
                    }
                    tvSliderLows[i]?.text = nutritionalInformation[lowKeyList[i]]!!.toString()
                    tvSliderHighs[i]?.text = nutritionalInformation[highKeyList[i]]!!.toString()
                }
                else -> {
                    sliders[i]?.apply {
                        labelBehavior = LabelFormatter.LABEL_GONE
                        valueTo = maxNutritionList[i]
                        values = listOf(0.0f, maxNutritionList[i])
                    }
                    tvSliderLows[i]?.text = "0"
                    tvSliderHighs[i]?.text = ceil(maxNutritionList[i]).toInt().toString()
                }
            }
        }
    }

    private fun saveCurrentSetting(
        arr: List<List<Float>?>
    ) {
        for (i in arr.indices) {
            if (arr[i]?.size == 2) {
                nutritionalInformation[lowKeyList[i]] = arr[i]!![0].toInt()
                nutritionalInformation[highKeyList[i]] = arr[i]!![1].toInt()
            } else {
                nutritionalInformation[lowKeyList[i]] = 0
                nutritionalInformation[highKeyList[i]] = 0
            }
        }
    }

    fun setSliderValue(
        maxProtein: Float,
        maxFat: Float,
        maxSugar: Float,
        nutritionalInformation: HashMap<String, Int>
    ) {
        this.maxNutritionList = arrayOf(maxProtein, maxFat, maxSugar)
        this.nutritionalInformation = nutritionalInformation
    }

    fun fragmentFinish() {
        listener.frameLayoutGone()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val backPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentFinish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FragmentSettingMain().apply {
                arguments = Bundle().apply {
                }
            }
    }

    interface FragmentSettingListener {
        fun frameLayoutGone()
        fun updateSettingImmediately(nutritionalInformation: HashMap<String, Int>)
    }
}


