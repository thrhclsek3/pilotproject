package org.jeonfeel.pilotproject1.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import org.jeonfeel.pilotproject1.data.database.dao.FavoriteDao
import org.jeonfeel.pilotproject1.data.database.entity.Favorite
import javax.inject.Singleton


@Database(entities = [Favorite::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favoriteDao(): FavoriteDao

    companion object {
        private var instance: AppDatabase? = null

        fun createDbInstance(@ApplicationContext context: Context) {
//            synchronized(AppDatabase::class) {
//                if (instance == null) {
//                    instance =
//            }
        }

        fun getDbInstance(): AppDatabase {
            return instance!!
        }
    }
}