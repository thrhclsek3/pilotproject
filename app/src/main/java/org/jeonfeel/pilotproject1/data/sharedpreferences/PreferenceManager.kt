package org.jeonfeel.pilotproject1.data.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import org.jeonfeel.pilotproject1.R
import java.lang.reflect.Array.set
import javax.inject.Inject
import javax.inject.Singleton


object PreferenceManager {
    private const val CAFFEINE_KEY = "isCaffeine"
    private const val SORT_KEY = "sortKind"

    const val SORT_LOW_KCAL = 0
    const val SORT_HIGH_KCAL = 1
    const val SORT_BASIC = 2
    const val CONTAINS_CAFFEINE = true
    const val NOT_CONTAINS_CAFFEINE = false

    private lateinit var sharedPreferencesInstance: SharedPreferences

    fun create(context: Context) {
        sharedPreferencesInstance = context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )
    }

    var isCaffeine: Boolean
        set(value) {
            setPreferenceValue(CAFFEINE_KEY, value)
        }
        get() {
            return sharedPreferencesInstance.getBoolean(CAFFEINE_KEY, false)
        }

    var sortKind: Int
        set(value) {
            setPreferenceValue(SORT_KEY, value)
        }
        get() {
            return sharedPreferencesInstance.getInt(SORT_KEY, -1)
        }

    private fun setPreferenceValue(key: String, value: Any) {
        when (value) {
            is Int -> setInt(key, value)
            is Boolean -> setBoolean(key, value)
        }
    }

    private fun setBoolean(key: String, isCaffeine: Boolean) {
        sharedPreferencesInstance.edit()
            .putBoolean(key, isCaffeine)
            .apply()
    }

    private fun setInt(key: String, sortKind: Int) {
        sharedPreferencesInstance.edit().putInt(key, sortKind).apply()
    }

    fun resetPreference() {
        sharedPreferencesInstance.edit().putInt(SORT_KEY, SORT_BASIC).apply()
        sharedPreferencesInstance.edit().putBoolean(CAFFEINE_KEY, NOT_CONTAINS_CAFFEINE).apply()
    }
}

