package org.jeonfeel.pilotproject1.di

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import androidx.core.content.ContextCompat
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import org.jeonfeel.pilotproject1.data.database.AppDatabase
import org.jeonfeel.pilotproject1.data.remote.api.RetrofitService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class SingletonModule {

    @Qualifier
    annotation class OpenObjRetrofit

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "AppDatabase"
        )
            .build()
    }

    @Singleton
    @Provides
    @OpenObjRetrofit
    fun provideOpenObjRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("http://dpms.openobject.net:4002/")
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    @Singleton
    @Provides
    fun provideOpenObjService(@SingletonModule.OpenObjRetrofit retrofit: Retrofit): RetrofitService {
        return retrofit.create(RetrofitService::class.java)
    }

//    @Singleton
//    @Provides
//    fun provideNetworkState(@ApplicationContext context: Context): Network? {
//
//    }
}