package org.jeonfeel.pilotproject1.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import org.jeonfeel.pilotproject1.data.database.AppDatabase
import org.jeonfeel.pilotproject1.data.remote.api.RetrofitService
import org.jeonfeel.pilotproject1.repository.MainRepository
import retrofit2.Retrofit

@Module
@InstallIn(ActivityRetainedComponent::class)
class ActivityRetainedModule {

    @Provides
    fun provideMainRepository(db: AppDatabase, retrofitService: RetrofitService): MainRepository {
        return MainRepository(db,retrofitService)
    }
}