package org.jeonfeel.pilotproject1.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import org.jeonfeel.pilotproject1.data.database.AppDatabase
import org.jeonfeel.pilotproject1.repository.MainRepository
import org.jeonfeel.pilotproject1.view.adapter.ViewPagerAdapter

@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {

    @Provides
    fun provideViewPagerAdapter(@ActivityContext context: Context): ViewPagerAdapter {
        return ViewPagerAdapter(context)
    }
}